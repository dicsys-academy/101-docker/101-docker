# 101 - Docker

# Link a Slack
https://join.slack.com/t/dicsyshablar/shared_invite/zt-1w4tb8tqb-oJPMAQoQ2MlBxgoGvJBU4g

# Curso Introductorio a Docker

## Sección 1: Introducción a Docker
¡Bienvenidos al curso introductorio a Docker! En esta sección, obtendremos una visión general de Docker y su importancia en el desarrollo de aplicaciones.

### ¿Qué es Docker?
Docker es una plataforma de código abierto que permite automatizar el despliegue y la administración de aplicaciones en entornos virtualizados llamados contenedores. Los contenedores proporcionan un entorno aislado y portátil para ejecutar aplicaciones de manera consistente en diferentes sistemas.

### Ventajas de usar Docker
- **Portabilidad:** Los contenedores Docker se pueden ejecutar en cualquier sistema operativo compatible, lo que facilita el desarrollo y la distribución de aplicaciones.
- **Aislamiento:** Cada contenedor ejecuta su propia instancia de una aplicación, lo que garantiza que no haya conflictos entre diferentes componentes o dependencias.
- **Escalabilidad:** Docker facilita la replicación y escalado de aplicaciones, ya que se pueden crear múltiples instancias de contenedores de manera sencilla.

## Sección 2: Comandos Básicos de Docker
En esta sección, aprenderemos los comandos básicos de Docker para administrar y trabajar con contenedores.

### Instalación de Docker
Para utilizar Docker, primero debemos instalarlo en nuestro sistema. Puedes seguir las instrucciones de instalación según tu sistema operativo en [este enlace](https://docs.docker.com/get-docker/).

### Comandos Esenciales
- `docker pull`: Descarga una imagen de Docker desde un registro.
- `docker run`: Crea y ejecuta un contenedor a partir de una imagen.
- `docker ps`: Lista los contenedores en ejecución.
- `docker stop`: Detiene un contenedor en ejecución.
- `docker rm`: Elimina un contenedor.
- `docker images`: Lista las imágenes de Docker disponibles en el sistema.
- `docker rmi`: Elimina una imagen de Docker.

### Ejemplo: Creando y Ejecutando un Contenedor
Para crear y ejecutar un contenedor a partir de una imagen, utiliza el siguiente comando:

```bash
docker run -d --name mi-contenedor nginx
```

Este comando crea un nuevo contenedor utilizando la imagen de Nginx. El contenedor se ejecutará en segundo plano (`-d`) y se le asignará el nombre "mi-contenedor". Ahora puedes acceder a la aplicación Nginx ejecutándose en el contenedor a través de tu navegador web.

## Sección 3: Dockerfiles
En esta sección, aprenderemos a crear Dockerfiles, que son archivos de configuración utilizados para construir imágenes personalizadas de Docker.

### ¿Qué es un Dockerfile?
Un Dockerfile es un archivo de texto que contiene una serie de instrucciones para construir una imagen de Docker. Estas instrucciones especifican los pasos necesarios para configurar el entorno dentro del contenedor.

### Sintaxis Básica de un Dockerfile
A continuación se muestra un ejemplo de Dockerfile básico:

```Dockerfile
# Indicar la imagen base
¡Disculpa por la interrupción! Aquí tienes la continuación del curso introductorio a Docker.

```Dockerfile
# Indicar la imagen base
FROM ubuntu:latest

# Actualizar el sistema operativo
RUN apt-get update && apt-get upgrade -y

# Instalar dependencias
RUN apt-get install -y python3

# Copiar archivos al contenedor
COPY mi_app.py /app/

# Establecer directorio de trabajo
WORKDIR /app

# Exponer puerto
EXPOSE 8080

# Comando de inicio
CMD ["python3", "mi_app.py"]
```

En este ejemplo de Dockerfile, cada instrucción tiene un propósito específico:
- `FROM`: Especifica la imagen base que se utilizará para construir la nueva imagen. En este caso, estamos utilizando la última versión de Ubuntu.
- `RUN`: Ejecuta comandos en el entorno del contenedor durante el proceso de construcción de la imagen. Aquí, estamos actualizando el sistema operativo y luego instalando Python 3.
- `COPY`: Copia archivos del sistema de archivos local al sistema de archivos del contenedor. En este caso, estamos copiando un archivo llamado `mi_app.py` al directorio `/app` dentro del contenedor.
- `WORKDIR`: Establece el directorio de trabajo dentro del contenedor. Todas las rutas relativas se evaluarán en relación con este directorio.
- `EXPOSE`: Indica el puerto en el que el contenedor escuchará conexiones.
- `CMD`: Especifica el comando que se ejecutará automáticamente cuando se inicie un contenedor basado en esta imagen. En este caso, se ejecutará el archivo `mi_app.py` utilizando Python 3.

### Ejemplo: Construyendo una Imagen con Dockerfile
Para construir una imagen utilizando un Dockerfile, utiliza el siguiente comando:

```bash
docker build -t mi-imagen:1.0 .
```

Este comando busca un Dockerfile en el directorio actual (`.`) y construye una nueva imagen con la etiqueta `mi-imagen:1.0`. Puedes utilizar esta imagen para crear y ejecutar contenedores personalizados.

## Sección 4: Docker Compose
En esta sección, aprenderemos a utilizar Docker Compose para definir y gestionar aplicaciones multi-contenedor.

### ¿Qué es Docker Compose?
Docker Compose es una herramienta que permite definir y administrar aplicaciones multi-contenedor utilizando un archivo de configuración YAML.

### Sintaxis Básica de Docker Compose
A continuación se muestra un ejemplo de archivo `docker-compose.yml` básico:

```yaml
version: '3'
services:
  web:
    build: .
    ports:
      - 8080:80
    volumes:
      - ./app:/app
  db:
    image: mysql:latest
    environment:
      - MYSQL_ROOT_PASSWORD=secret
```

En este ejemplo, definimos dos servicios: `web` y `db`. Cada servicio tiene su propia configuración, como la construcción de una imagen (`build`), la exposición de puertos (`ports`), los volúmenes compartidos (`volumes`), y las variables de entorno (`environment`).

### Ejemplo: Ejecutando una Aplicación con Docker Compose
Para ejecutar una aplicación utilizando Docker Compose, crea un archivo `docker-compose.yml` en el directorio de tu proyecto y utiliza el siguiente comando:

```bash
docker-compose up
```

Este comando leerá el archivo `docker-compose.yml` y creará y ejecutará todos los contenedores definidos en él. Los contenedores se ejecutarán en segundo plano y podrás acceder a ellos según las configuraciones definidas en el archivo `docker-compose.yml`.

## Sección 5: Volúmenes en Docker
En esta sección, aprenderemos sobre los volúmenes en Docker, que nos permiten persistir datos y compartir información entre contenedores y el sistema host.

### ¿Qué son los volúmenes en Docker?
Los volúmenes en Docker son directorios o archivos especiales que existen fuera del sistema de archivos del contenedor y se utilizan para almacenar y compartir datos. Los volúmenes permiten persistir datos incluso cuando los contenedores se detienen o se eliminan.

### Tipos de Volúmenes
Docker ofrece diferentes opciones para trabajar con volúmenes. Algunas de las más comunes son:

- **Volúmenes de contenedor**: Son volúmenes que se crean y administran dentro de Docker y se asocian a un contenedor específico.
- **Volúmenes de host**: Permiten montar directorios o archivos del sistema host dentro del contenedor, lo que facilita compartir datos entre ambos.
- **Volúmenes anónimos**: Son volúmenes que Docker gestiona automáticamente y que están asociados a un contenedor, pero no tienen un nombre explícito.
- **Volúmenes con nombre**: Son volúmenes que tienen un nombre explícito y se pueden compartir entre múltiples contenedores.

### Ejemplo: Uso de Volúmenes
A continuación se muestra un ejemplo de cómo utilizar volúmenes en un archivo `docker-compose.yml`:

```yaml
version: '3'
services:
  web:
    build: .
    ports:
      - 8080:80
    volumes:
      - mydata:/app/data
  db:
    image: mysql:latest
    environment:
      - MYSQL_ROOT_PASSWORD=secret
    volumes:
      - dbdata:/var/lib/mysql
volumes:
  mydata:
  dbdata:
```

En este ejemplo, hemos definido dos volúmenes con nombre: `mydata` y `dbdata`. El servicio `web` utiliza el volumen `mydata` para persistir los datos en el directorio `/app/data` dentro del contenedor. El servicio `db` utiliza el volumen `dbdata` para persistir los datos de la base de datos MySQL en el directorio `/var/lib/mysql`.

### Gestión de Volúmenes
A continuación se muestran algunos comandos útiles para gestionar volúmenes en Docker:

- `docker volume create <nombre>`: Crea un nuevo volumen con el nombre especificado.
- `docker volume ls`: Lista todos los volúmenes disponibles en el sistema.
- `docker volume inspect <nombre>`: Proporciona información detallada sobre un volumen específico.
- `docker volume rm <nombre>`: Elimina un volumen específico.

Recuerda que los volúmenes son una herramienta poderosa para persistir y compartir datos en Docker. Utilízalos de acuerdo a tus necesidades y asegúrate de comprender cómo funcionan en diferentes situaciones.

## Sección 6: Manejo de Redes en Docker
En esta sección, aprenderemos sobre el manejo de redes en Docker, que nos permite conectar y comunicar contenedores entre sí y con el mundo exterior.

### ¿Qué es el manejo de redes en Docker?
El manejo de redes en Docker permite crear y administrar redes virtuales para los contenedores. Estas redes facilitan la comunicación entre los contenedores y ofrecen opciones de conectividad con el host y otras redes externas.

### Tipos de Redes en Docker
Docker ofrece diferentes tipos de redes para satisfacer diferentes necesidades. Algunos tipos comunes de redes en Docker son:

- **Bridge**: Es la red predeterminada utilizada cuando se crea un contenedor. Los contenedores conectados a la misma red Bridge pueden comunicarse entre sí mediante nombres de host.
- **Host**: El contenedor utiliza la red del host directamente, sin aislamiento ni traducción de puertos. Esto permite que el contenedor utilice las interfaces de red del host directamente.
- **Overlay**: Permite conectar contenedores que se ejecutan en diferentes hosts y facilita la creación de clústeres de Docker distribuidos.
- **Macvlan**: Permite asignar direcciones MAC individuales a cada contenedor, lo que proporciona conectividad directa a la red física subyacente.

### Ejemplo: Creación de una Red en Docker
A continuación se muestra un ejemplo de cómo crear una red en Docker mediante el uso de Docker Compose:

```yaml
version: '3'
services:
  web:
    build: .
    ports:
      - 8080:80
    networks:
      - mynetwork
  db:
    image: mysql:latest
    environment:
      - MYSQL_ROOT_PASSWORD=secret
    networks:
      - mynetwork
networks:
  mynetwork:
```

En este ejemplo, hemos definido una red llamada `mynetwork` y hemos asignado los servicios `web` y `db` a esa red. Esto permitirá que ambos contenedores se comuniquen entre sí a través de la red `mynetwork`.

### Gestión de Redes en Docker
A continuación se muestran algunos comandos útiles para gestionar redes en Docker:

- `docker network create <nombre>`: Crea una nueva red con el nombre especificado.
- `docker network ls`: Lista todas las redes disponibles en el sistema.
- `docker network inspect <nombre>`: Proporciona información detallada sobre una red específica.
- `docker network connect <red> <contenedor>`: Conecta un contenedor existente a una red específica.
- `docker network disconnect <red> <contenedor>`: Desconecta un contenedor de una red específica.

Recuerda que el manejo de redes en Docker te permite establecer conexiones entre contenedores y con el mundo exterior. Utilízalo de acuerdo a tus necesidades y asegúrate de comprender cómo funcionan los diferentes tipos de redes.

## Conclusión

¡Felicitaciones por completar el curso introductorio a Docker! Ahora tienes los conocimientos fundamentales para utilizar Docker en el desarrollo de aplicaciones. Recuerda practicar y explorar más para aprovechar al máximo esta poderosa herramienta.

Espero que este curso te haya sido útil. Si tienes alguna otra pregunta o necesitas más información, no dudes en preguntar. ¡Buena suerte en tu camino como desarrollador de Docker!

